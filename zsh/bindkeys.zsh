# To see the key combo you want to use just do:
# cat > /dev/null
# And press it 

bindkey "^A"   beginning-of-line                 # Ctrl-a  
bindkey "^B"   backward-char                     # Ctrl-b
bindkey "^E"   end-of-line                       # Ctrl-e
bindkey "^F"   forward-char                      # Ctrl-f
bindkey "^D"   delete-char                       # Ctrl-d
bindkey "^K"   kill-whole-line                   # Ctrl-k
bindkey "^N"   down-line-or-search               # Ctrl-n
bindkey "^P"   up-line-or-search                 # Ctrl-p
bindkey '^[[3~' delete-char                      # Supr
bindkey '^[[H' beginning-of-line                 # Home
bindkey '^[[F' end-of-line                       # End
bindkey '^[[A' history-beginning-search-backward # Up arrow
bindkey '^[[B' history-beginning-search-forward  # Down arrow
bindkey '^R' history-incremental-search-backward # Ctrl-r
bindkey '^T' expand-or-complete-prefix           # Ctrl-t (Autocompletes in the midle of a string)
