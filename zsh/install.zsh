#!/bin/env zsh

packages=(trash-cli bind-tools)

toInstall=()
for package in $packages; do
  pacman -Qi $package 2> /dev/null > /dev/null
  if [[ $? -ne 0 ]]; then; toInstall+=$package; fi
done

if [[ ${#toInstall[@]} -ne 0 ]]; then
  sudo pacman -S $toInstall
fi

CONFIGPATH=$(dirname $(readlink -f $0))

cp -Ra $CONFIGPATH $XDG_CONFIG_HOME
ln -sf $XDG_CONFIG_HOME/zsh/zshenv $HOME/.zshenv
ln -sf $XDG_CONFIG_HOME/zsh/zshrc $HOME/.zshrc

typeset -A plugins
plugins[zsh-autosuggestions]="https://github.com/zsh-users/zsh-autosuggestions.git"

for plugin in ${(@k)plugins}; do
   if [ ! -d "$XDG_CONFIG_HOME/zsh/$plugin" ]; then
        echo "${plugin}: Installing"
        git clone $plugins[$plugin] "$XDG_CONFIG_HOME/zsh/$plugin"
        rm -rf "$XDG_CONFIG_HOME/zsh/$plugin/.git"
        if [[ $? -ne 0 ]]; then echo "ERROR"; fi
   else
        echo "${plugin}: The plugin is already installed, skip."
   fi
done
