#!/bin/zsh

set -e

# Key to import. The recommended way of setting the key
# is not by using this variable, but by passing the key
# in an environment variable named KEY_ID
key_id="REPLACE WITH YOUR KEY"

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_DIRS=/usr/local/share/:/usr/share
export XDG_CONFIG_DIRS=/etc/xdg
export XDG_CACHE_HOME="$HOME/.cache"
export ZSH_CONFIG_HOME="$XDG_CONFIG_HOME/zsh"

export GNUPGHOME="$XDG_CONFIG_HOME/gnupg"

# Install and configure yubikey

[[ "$KEY_ID" = "" ]]  && KEY_ID=$key_id

[[ "$KEY_ID" = "REPLACE WITH YOUR KEY" ]] && echo "Configure your key before running the script" && exit 1

echo "Configuring yubikey"
packages=(yubikey-manager)

echo "Installing packages"
toInstall=()
for package in $packages; do
  pacman -Qi $package 2> /dev/null > /dev/null
  if [[ $? -ne 0 ]]; then; toInstall+=$package; fi
done

if [[ ${#toInstall[@]} -ne 0 ]]; then
  sudo pacman -S $toInstall
fi

[[ -d $GNUPGHOME ]] || mkdir -p $GNUPGHOME

cat <<EOF > $GNUPGHOME/gpg-agent.conf
enable-ssh-support
pinentry-program /usr/bin/pinentry-qt
default-cache-ttl 60
max-cache-ttl 120

# Useful options to debug
#debug-level expert
#debug-pinentry
#debug 1024
#lc-messages en_US.UTF-8
EOF

cat <<EOF > $GNUPGHOME/gpg.conf
personal-cipher-preferences AES256 AES192 AES CAST5
personal-digest-preferences SHA512 SHA384 SHA256 SHA224
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed
cert-digest-algo SHA512
s2k-digest-algo SHA512
s2k-cipher-algo AES256
charset utf-8
fixed-list-mode
no-comments
no-emit-version
keyid-format 0xlong
list-options show-uid-validity
verify-options show-uid-validity
with-fingerprint
require-cross-certification
use-agent
EOF

cat <<EOF > $GNUPGHOME/scdaemon.conf
pcsc-driver /usr/lib/libpcsclite.so
card-timeout 5
disable-ccid
EOF

find $GNUPGHOME -type d -exec chmod 700 {} \;
find $GNUPGHOME -type f -exec chmod 600 {} \;

echo "Starting services"
sudo systemctl enable pcscd
sudo systemctl start pcscd

gpg --recv-keys $KEY_ID
echo "NOTE: Remember to trust the key"

echo "Finished"
